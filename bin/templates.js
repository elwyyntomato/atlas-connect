var stashed = require('stashed')
  , path = require('path');

var argv = require('optimist')
  .describe('n','No colors')
  .alias('n','no-colors')
  .default('n',false)
  .argv;

exports = module.exports = stashed({
  templateDir: path.join(__dirname, '..', 'templates'),
  noColors: argv.n
});

