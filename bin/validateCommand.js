var path = require('path')
  , _ = require('lodash')
  , validCmds = require(path.join(__dirname, 'validCommands.json'))
  , pkg = require(path.join(__dirname, '..', 'package.json'))
  , render = require(path.join(__dirname, 'templates'));

exports = module.exports = function validateCommand(argv){
  if(argv.v) {
    console.log(render('version', pkg));
    return true;
  }

  if(argv._.length === 0 || argv.h){
    throw "";
  }

  var cmd = argv._[0];
  var params = argv._.slice(1,argv._.length);
  var cmdIsValid = false;

  if(cmd.toLowerCase() === "help") {
    throw "";
  }

  var validCmd = _.where(validCmds, {cmd: cmd.toLowerCase()})
  if(validCmd.length > 0) {
    checkParams(params, validCmd[0])
  } else {
    throw render('command-not-recognized', {cmd:cmd});
  }
}

function checkParams(params, cmdSpec){
  var requiredParams = _.where(cmdSpec.params,{required: true});

  if (params.length < requiredParams.length) {
    throw render('requires-parameters',{
      cmd: cmdSpec.cmd,
      params: cmdSpec.params
    });
  }
}