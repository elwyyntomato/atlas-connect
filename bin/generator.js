var fs = require("fs")
  , temp = require('temp')
  , https = require("https")
  , AdmZip = require('adm-zip')
  , read = require('read')
  , rimraf = require('rimraf')
  , path = require('path')
  , render = require(path.join(__dirname, 'templates'))
  , ProgressBar = require('progress');


exports = module.exports = function(name, template, cb){
  var dlUrl = "https://bitbucket.org/atlassian/atlassian-connect-express-template/get/master.zip";
  if(!template) {
    template = "default";
  }
  switch(template){
    case "default":
      break;
    case "hipchat":
      dlUrl = "https://bitbucket.org/atlassian/atlassian-connect-express-template/get/"+template+".zip";
      break;
    default:
      console.log(render('template-not-found',{template: template}));
      return;
  }

  https.get(dlUrl, function(res){
    var data = [], dataLen = 0;

    var len = parseInt(res.headers['content-length'] || 1000, 10);
    var bar = new ProgressBar('Downloading template [:bar] :percent :etas', {
        complete: '='
      , incomplete: ' '
      , width: 20
      , total: len
    });

    res.on('data', function(chunk){
      data.push(chunk);
      dataLen += chunk.length;
      bar.tick(chunk.length);
    }).on('end', function(){
      if (res.statusCode == 404) {

        return;
      }
      var buf = new Buffer(dataLen);

      for (var i=0, len = data.length, pos = 0; i < len; i++) {
          data[i].copy(buf, pos);
          pos += data[i].length;
      }

      var zip = new AdmZip(buf);
      var zipEntries = zip.getEntries();
      var zipDir = "";

      console.log("Downloading template [===================] 100% 0.0s");

      fs.exists(name,function(err){
        if (err){
          read({
            prompt: render('project-exists-overwrite',{name: name}),
            default: "n"
          }, function(er, overwrite){
            if(overwrite.toLowerCase() === "y"){
              rimraf(name,function(err){
                if(!err){
                  createScaffold();
                }
              });
            }
          });
        } else {
          createScaffold();
        }

        function createScaffold(){
          var outDir = path.join(process.cwd(),name);
          temp.mkdir(name, function(err,dirPath){
            zip.extractAllTo(dirPath, true);
            zipEntries.forEach(function(zipEntry){
              zipDir = zipEntry.entryName.split('/')[0];
              var path = [name,zipEntry.entryName.split('/')
                .slice(1).join('/')].join('/');
              console.log(("  " + path).yellow);
            });
            // Node's renameSync fails when moving files across partitions:
            // http://stackoverflow.com/questions/4568689/how-do-i-move-file-a-to-a-different-partition-in-node-js
            // So, commenting this out for now in favor of a crude spawn
            // fs.renameSync(path.join(dirPath,zipDir), outDir);
            require('child_process').spawn('cp', ['-r', path.join(dirPath,zipDir), outDir]);
            console.log(render('project-created',{name: name}));
            cb();
          });
        }
      });
    });
  });
};