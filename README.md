# atlas-connect

A simple CLI for the [`atlassian-connect-express`](https://bitbucket.org/atlassian/atlassian-connect-express) library. In short, `atlas-connect` makes it easy to create [Atlassian Connect](http://connect.atlassian.com) based Add-ons.

You can install me with:

    npm i -g atlas-connect

## Usage

`atlas-connect` doesn't attempt to do too much. Mainly, it's a helper tool for creating a add-on project scaffold.

    Usage: atlas-connect [-h|--help] <command> [<args>]

    The most commonly used atlas-connect commands are:
        keygen      Generate public and private RSA keys
        new         Generate a new Atlassian Connect Add-on scaffold

    Options:
      -h, --help     Show usage
      -v, --version  Show version

The `atlas-connect` simplifies the process of building an Atlassian Add-on using the [Express](http://expressjs.com) framework through:

* A project scaffold generator with great default settings
* An OAuth public/private key pair generation

On top of that, the generated scaffold bundles the [atlassian-connect-express](https://bitbucket.org/atlassian/atlassian-connect-express) package which greatly simplifies the process of creating remote add-ons.

## Getting Help or Support

The `atlassian-connect-express` tools are currently experimental. With that said, feel free to [report issues](https://ecosystem.atlassian.net/browse/ACEJS). If you want to learn more about Atlassian Connect, you can visit <http://connect.atlassian.com>.

## Contributing

Even though this is just an exploratory project at this point, it's also open source [Apache 2.0](https://bitbucket.org/atlassian/node-atlas-connect/src/master/LICENSE.txt). So, please feel free to fork and send us pull requests.